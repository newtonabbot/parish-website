
;; (require 'package)
;; (package-initialize)
;; (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
;; (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; (package-refresh-contents)
;; (package-install 'org-plus-contrib)
;; (package-install 'htmlize)

(require 'org)
(require 'ox-publish)

;; setting to nil, avoids "Author: x" at the bottom
(setq user-full-name nil)

(setq org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-toc nil)

(setq org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-validation-link nil
      org-html-doctype "html5")

(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js" "woff" "html" "pdf" "docx" "_redirects"))
  "File types that are published as static files.")

(setq org-html-head-include-default-style nil)

;; IS the following quite right?
;; Anyway, this sort of works for building the nav thing, but the nested pages are in a different location
;; I should probably just include the 'index' file in the parish-groups directory too
;; change the top nav

;; !!! Might be needed for parish groups - to be continued...
;; (org-publish-initialize-cache "parish-website")


;; this stuff is getting closer, but the relative links always cause problems.
;; The top links aren't going to want to be absolute. What can I do about it all?
;; I want it to work with all relative links ideally...


;; generate the navigation thingy for the parish groups
(defun make-sitemap ()
  (with-temp-file "parish-groups/navigation.org"
    (mapcar (lambda (file)
              (unless (string-equal file "navigation.org")
                (let ((title (org-publish-find-title
                              (concat "parish-groups/" file) (list "parish-website"))))
                  (when title
                    (insert "- [[file:")
                    (insert file)
                    (insert "][")
                    (insert title)
                    (insert "]]")
                    (insert "
")))))
            (directory-files "parish-groups" nil ".org$"))))



(setq org-publish-project-alist
      (list
       (list "site-org"
             :base-directory "."
             :base-extension "org"
             :recursive t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public"
             :exclude (regexp-opt '("README" "draft" "incoming"))
             ;; :auto-sitemap t
             
             ;; I think I could use these bits
             ;; !!! link the image to the homepage so we always have a way to get back
             ;; I wonder if I can make a preamble part form an org file so I can link in the top nav w/o
             ;; having to switch it to HTML
             ;; alternatively should I just redo this using the site-map facility. It should work
             :html-head-extra "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"/><link rel=\"stylesheet\" type=\"text/css\" href=\"default.css\"/>
<meta name='viewport' content='width=840'>

"
             ;; see https://thibaultmarin.github.io/blog/posts/2016-11-13-Personal_website_in_org.html#org3ae0edc

             :html-preamble "<img src='st-joseph-icon.jpg' id='st-josephs-icon' width='150px'>
<h2>St. Joseph's</h2>
<p>Roman Catholic Parish<br>Newton Abbot</p>
<h3><a href='http://universalis.com/today.htm'><!-- Monday of week 13 in Ordinary Time --></a></h3>
"
             :html-postamble "<p>St. Joseph's is part of the Plymouth R.C. Diocese: Reg. Charity No: 213227</p><p>This website is <a href='https://gitlab.com/ppymdjr/parish-website/'>published via GitLab</a> using GNU Emacs's org-mode (<a href='https://gitlab.com/ppymdjr/parish-website/blob/master/README.org'>more info</a>)</p>"
             )
       (list "site-static"
             :base-directory "."
             :exclude "public/"
             :base-extension site-attachments
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "site" :components '("site-org"))))

(provide 'publish)
;;; publish.el ends here
