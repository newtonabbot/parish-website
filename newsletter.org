#+TITLE: Newsletter
#+INCLUDE: header.org
#+OPTIONS: ^:nil

* Newsletter PDFs
Click the following links to download PDF versions of the most recent newsletters.

- [[file:newsletter-pdfs/2022-01-09.pdf][9/1/2022]]
- [[file:newsletter-pdfs/christmas-day-bulletin-2021.pdf][Christmas Day 2021]]
- [[file:newsletter-pdfs/2021-12-12.pdf][19/12/2021]]
- [[file:newsletter-pdfs/2021-12-12.pdf][12/12/2021]]
- [[file:newsletter-pdfs/2021-12-05.pdf][5/12/2021]]
- [[file:newsletter-pdfs/2021-11-28-1.jpg][28/11/2021 - page 1]], [[file:newsletter-pdfs/2021-11-28-1.jpg][28/11/2021 - page 2]]
- [[file:newsletter-pdfs/2021-11-21.pdf][21/11/2021]]
- [[file:newsletter-pdfs/2021-11-14.pdf][14/11/2021]]
- [[file:newsletter-pdfs/2021-11-07.pdf][7/11/2021]]
- [[file:newsletter-pdfs/2021-10-31.pdf][31/10/2021]]
- [[file:newsletter-pdfs/2021-10-24.pdf][24/10/2021]]
- [[file:newsletter-pdfs/2021-10-17.pdf][17/10/2021]]
- [[file:newsletter-pdfs/2021-10-10.pdf][10/10/2021]]
- [[file:newsletter-pdfs/2021-10-03.pdf][3/10/2021]]
- [[file:newsletter-pdfs/2021-09-26.pdf][26/9/2021]]
- [[file:newsletter-pdfs/2021-09-19.pdf][19/9/2021]]
- [[file:newsletter-pdfs/2021-09-12.pdf][12/9/2021]]
- [[file:newsletter-pdfs/2021-09-05.pdf][5/9/2021]]
- [[file:newsletter-pdfs/2021-08-29.pdf][29/8/2021]]
- [[file:newsletter-pdfs/2021-08-22.pdf][22/8/2021]]
- [[file:newsletter-pdfs/2021-08-15.pdf][15/8/2021]]
- [[file:newsletter-pdfs/2021-08-08.pdf][8/8/2021]]
- [[file:newsletter-pdfs/2021-08-01.pdf][1/8/2021]]

* Newsletter
** 9/1/2022

[[file:newsletter-pdfs/2022-01-09.pdf][This weeks newsletter]]

* Blank
:PROPERTIES:
:CUSTOM_ID: hide1
:END:

#+BEGIN_EXPORT html
<!-- try and make this not html - convert to org -->
<div style='width:300px;'>
&nbsp;
</div>
#+END_EXPORT
