#+INCLUDE: header.org
#+title: About Our Parish

* About Our Parish

*Parochial Administrator*: Father Anthony Onouha\\
*Priests*: Father Joseph Nwiboko, Father Apollos Mejuru and Mgr George Hay (retired)\\
*Deacons*: Paul Hewson, Gerard Nosowski and Tim van Kroonenburg


** Address
96 Queen Street, \\
Newton Abbot, \\
Devon TQ12 2ET\\

*Tel*: 01626 365231 
*Email*: newtonabbot@prcdtr.org.uk

** Parish School
St. Joseph's RC Primary School

Coombeshead Road, 
Highweek,
Newton Abbot, 
TQ12 1PT 

*Tel*: 01626 352559
*Website*: [[http://www.st-josephs-primary.devon.sch.uk][www.st-josephs-primary.devon.sch.uk]]
*Email*: admin@sjna.uk

** Safeguarding Children & Vulnerable Adults

Parish Safeguarding Representatives: 
Nickola Scagell 
Tel: 01626 361694

Deborrah Back
Tel: 01626 353322

Monica Trist
Tel: 01626 331319
