

;; It might be easier to just fire up a quick web server to resolve issues with relative links
;; There are other ways to do this (including from emacs I expect), but this will work...

(ql:quickload :aserve)
(net.aserve:start :port 8080)
(net.aserve:publish-directory :prefix "/parish-website" :destination "./public/")
