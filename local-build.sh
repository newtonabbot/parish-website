

# in running with emacs via docker this uses the current working directory as /root/ and hence puts in a .emacs.d with the installed packages
# in order to do a full clean build it is necessary to remove this
rm -rf public/*
rm -rf .org-timestamps

# /Applications/Emacs.app/Contents/MacOS/Emacs --batch --load publish.el --eval '(org-publish-all 1)'
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

docker run --rm -v $DIR:/root/ iquiw/alpine-emacs emacs --batch --no-init-file --load publish.el --funcall org-publish-all

# this will publish to a netlify site I have set up. I guess this should work from gitlab too once I update that. I need to work out pipeline stages for it
# !!! NOTE: NETLIFY_AUTH_TOKEN must be set for this to work. That can be got from the netlify web front end AND I have set it as a variable in the gitlab config
# this means that this command will work if run from gitlab
# docker run --rm -e NETLIFY_AUTH_TOKEN=$NETLIFY_AUTH_TOKEN -v $DIR:/root/ iquiw/alpine-emacs /bin/sh -c "emacs --batch --no-init-file --load publish.el --funcall org-publish-all && apk add --update nodejs && apk add npm && npm install netlify-cli -g && netlify deploy -s bf7ea40a-bd2a-48c1-bd14-87161b52a6e6 --dir=./public --prod"
