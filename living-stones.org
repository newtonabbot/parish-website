#+TITLE: Living Stones
#+INCLUDE: header.org

* Living Stones

#+ATTR_HTML: :class topic-image
file:images/living-stones.png

*Living Stones* is St Joseph's parish's response to Bishop Mark's call to 
‘Go therefore, make disciples of all the nations’ (Matthew 28:19).

We meet once a month as a Parish Evangelisation Team. 
We spend some time before the Blessed Sacrament before recounting stories of God’s work as He uses us to spread the Good News, bringing others to Jesus Christ.

Come and join us!

*Next meeting 19.30, Tuesday October 10th, St Josephs Church.*
